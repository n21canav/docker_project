# variable "path-manifests" {
#   type = string
#   default = "k8s_deployment/"
#   description = "Source directory of kubernets manifests"
# }

# locals {
#   deployments = {
#     // db
#     "db-depl" = "${var.path-manifests}db/db-deployment.yaml",
#     "db-pvc" = "${var.path-manifests}db/db-pvc.yaml"
#     "db-serv" = "${var.path-manifests}db/db-service.yaml",
#     //result
#     "result-depl" = "${var.path-manifests}result/result-deployment.yaml",
#     "result-serv" = "${var.path-manifests}result/result-service.yaml",
#     //seed
#     "seed-job" = "${var.path-manifests}seed/seed-job.yaml",
#     //vote
#     "vote-depl" = "${var.path-manifests}vote/vote-deployment.yaml",
#     "vote-serv" = "${var.path-manifests}vote/vote-service.yaml",
#     //worker
#     "worker-depl" = "${var.path-manifests}worker/worker-deployment.yaml"
#   }
# }


# resource "kubernetes_manifest" "app-depl" {
#   for_each = local.deployments

#   manifest = yamldecode(file(each.value))
# }