# Table of Content

- [Docker project](#docker-project)
- [Kubernetes project](#kubernetes-project)
- [Terraform project](#terraform-project)

# Docker project

To spin-up the project, simply do at the root of the project:  
```
docker compose up 
```

(The artifact registry is public for pull requests)

You can see the different service on these url :
For the voting service : 
http://localhost:8000/

For the result service : 
http://localhost:4000/

# Kubernetes project

To spin-up the project, simply do : 

```
cd .\k8s_deployment\
```

Then, if you're on windows : 
```
.\deploy-k8s.ps1    
```

And if you're on Linux (untested): 

```
.\deploy-k8s.sh 
```

You should then see all pods running : 
```
PS ...\docker_project\k8s_deployment> kubectl get pods 
NAME                                 READY   STATUS      RESTARTS   AGE
db-747698bf74-56g8d                  1/1     Running     0          3m46s
redis-5c8cd7c548-fffm7               1/1     Running     0          3m45s
result-8df8bb588-9q9k5               1/1     Running     0          3m45s
seed-job-j6dxl                       0/1     Error       0          3m45s
seed-job-kf8zh                       0/1     Completed   0          2m58s
vote-deployment-85b484d5cf-ns7cz     1/1     Running     0          3m45s
vote-deployment-85b484d5cf-wkxgp     1/1     Running     0          3m45s
vote-deployment-85b484d5cf-zwbc9     1/1     Running     0          3m45s
worker-deployment-6b64d454dd-tg974   1/1     Running     0          3m44s
```
And you can then access the service on a provided url, here is an exemple with Minikube : 
``` 
PS ...\docker_project\k8s_deployment> minikube service result  

|-----------|--------|-------------|---------------------------|
| NAMESPACE |  NAME  | TARGET PORT |            URL            |
|-----------|--------|-------------|---------------------------|
| default   | result |        4000 | http://192.168.49.2:30530 |
|-----------|--------|-------------|---------------------------|
🏃  Starting tunnel for service result.
|-----------|--------|-------------|------------------------|
| NAMESPACE |  NAME  | TARGET PORT |          URL           |
|-----------|--------|-------------|------------------------|
| default   | result |             | http://127.0.0.1:16377 |
|-----------|--------|-------------|------------------------|
🎉  Opening service default/result in default browser...
```


# Terraform project

## Objectives

The objective is to use Terraform to deploy the voting app.

## Part 1 mandatory - Docker

To spin-up the project, simply do : 

```
cd .\TerraDock\
terraform apply 
```

and put yes, you should then see :
```
Apply complete! Resources: 18 added, 0 changed, 0 destroyed.
```
You can see the different service on these url :
For the voting service : 
http://localhost:8000/

For the result service : 
http://localhost:4000/


## Part 2 mandatory - GKE and Kubernetes

To spin-up the project 

```
cd .\TerraDock\
```
First, make sure you download the json file from Google console for you terraform service account,
rename it key.json and put it inside the TerraDock folder. 
Then, you have to replace the target_service_account with your target service account in providers.tf.  
Finally, replace the project_id and others var if necessary in the terraform.tfvars. 

Then, do a first :
```
terraform apply 
```
This should create the k8s cluster on Google Cloud, and then, you can uncomment the deployment.tf and do again
```
terraform apply 
```
Your services should now be running. 

You can go to the service part in your Dashboard to see the ip address of the services. 