FROM python:3.8-slim

# Install apache2-utils
RUN apt-get update 
RUN apt-get install -y apache2-utils

# Copy application source code
WORKDIR /app
COPY . .


# Execute make-data.py to prepare data
RUN python make-data.py 

# Executed when starting the container
ENTRYPOINT ["sh","generate-votes-k8s.sh"]