# Recursively find all .yaml files and apply them
Get-ChildItem -Path . -Include *.yaml -Recurse | ForEach-Object {
    Write-Host "Applying $($_.FullName)"
    kubectl apply -f $_.FullName
}

# Complete
Write-Host "All YAML files have been applied."
