#!/bin/bash

# Recursively find all .yaml files and apply them
find . -type f -name "*.yaml" -print0 | while IFS= read -r -d $'\0' file; do
    echo "Applying $file"
    kubectl apply -f "$file"
done

# Complete
echo "All YAML files have been applied."
