// VOTE SERVICE

resource "docker_image" "vote" {
  name = "vote"
  build {
    context = "../vote/"
  }
}

resource "docker_container" "vote_1" {
  name  = "vote_1"
  image = docker_image.vote.image_id
  ports {
    internal = "5000"
    external = "5000"
  }
  networks_advanced {
    name = docker_network.frontend_network.name
  }
  networks_advanced {
    name = docker_network.backend_network.name
  }
  depends_on = [docker_container.worker]
}

resource "docker_container" "vote_2" {
  name  = "vote_2"
  image = docker_image.vote.image_id
  ports {
    internal = "5000"
    external = "5001"
  }
  networks_advanced {
    name = docker_network.frontend_network.name
  }
  networks_advanced {
    name = docker_network.backend_network.name
  }
  depends_on = [docker_container.worker]
}

resource "docker_container" "vote_3" {
  name  = "vote_3"
  image = docker_image.vote.image_id
  ports {
    internal = "5000"
    external = "5002"
  }
  networks_advanced {
    name = docker_network.frontend_network.name
  }
  networks_advanced {
    name = docker_network.backend_network.name
  }
  depends_on = [docker_container.worker]
}

// WORKER SERVICE
resource "docker_image" "worker" {
  name = "worker"
  build {
    context = "../worker/"
  }
  
}
resource "docker_container" "worker" {
  name  = "worker"
  image = docker_image.worker.image_id
  networks_advanced {
    name = docker_network.backend_network.name
  }
}

// RESULT SERVICE
resource "docker_image" "result" {
  name = "result"
  build {
    context = "../result/"
  }
}

resource "docker_container" "result" {
  name  = "result"
  image = docker_image.result.image_id
  ports {
    internal = "4000"
    external = "4000"
  }
  networks_advanced {
    name = docker_network.frontend_network.name
  }
  networks_advanced {
    name = docker_network.backend_network.name
  }
  depends_on = [docker_container.db]
}


// SEED SERVICE
resource "docker_image" "seed" {
  name = "seed"
  build {
    context = "../seed-data/"
  }
  depends_on = [ docker_container.nginx ]
}

resource "docker_container" "seed" {
  name  = "seed"
  image = docker_image.seed.image_id
  networks_advanced {
    name = docker_network.frontend_network.name
  }
  depends_on = [ docker_container.nginx ]
}


// Nginx SERVICE
resource "docker_image" "nginx" {
  name = "nginx"
}

resource "docker_container" "nginx" {
  name  = "nginx"
  image = docker_image.nginx.image_id
  ports {
    internal = "8000"
    external = "8000"
  }
  env = [
    "VOTE_SERVER_ADDR_1=vote_1:5000",
    "VOTE_SERVER_ADDR_2=vote_2:5001",
    "VOTE_SERVER_ADDR_3=vote_3:5002"
  ]
  volumes {
    host_path = "E:/IMT-Atlantrique/3A/UE_CLoud/docker_project/nginx/nginx.conf"
    container_path = "/tmp/nginx.conf"
  }
  command = ["/bin/bash", "-c", "envsubst < /tmp/nginx.conf > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]
  depends_on = [ 
    docker_container.vote_1,
    docker_container.vote_2,
    docker_container.vote_3
  ]
  networks_advanced {
    name = docker_network.frontend_network.name
  }
}

// Redis SERVICE
resource "docker_image" "redis" {
  name  = "redis:alpine"
}
resource "docker_container" "redis" {
    image = docker_image.redis.image_id
    name  = "redis"
    networks_advanced {
      name = docker_network.backend_network.name
    }
}

// Db SERVICE
resource "docker_image" "db" {
  name  = "postgres:alpine"
}

variable "POSTGRES_PASSWORD" {
  default = "postgres"
}

resource "docker_container" "db" {
    image = docker_image.db.image_id
    name  = "db"
    networks_advanced {
      name = docker_network.backend_network.name
    }  
    env = [
    "POSTGRES_PASSWORD=${var.POSTGRES_PASSWORD}"
    ]
    volumes {
      container_path = "/var/lib/postgresql/data"
    }
}

#FRONT END Network
resource "docker_network" "frontend_network" {
  name = "front-net"
}

#BACKEND Network
resource "docker_network" "backend_network" {
  name = "back-net"
}
